#include <linux/init.h>
#include <linux/init.h>
//#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h> /* printk() */
#include <linux/slab.h> /* kmalloc() */
#include <linux/fs.h> /* everything... */
#include <linux/errno.h> /* error codes */
#include <linux/types.h> /* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h> /* O_ACCMODE */
#include <linux/ioport.h>
#include <linux/delay.h>
//#include <asm/system.h> /* cli(), *_flags */
#include <asm/uaccess.h> /* copy_from/to_user */
#include <asm/io.h> /* inb, outb */

MODULE_LICENSE("Dual BSD/GPL");
void readIO(void);
short tmpword;
int addr = 707470992;
char buffer[512];
short drive=0x01;
int idx;

void readIO()
{
outb(0x1F3, 0x88);
msleep(1); // wait 1/250th of a second
tmpword = inb(0x1F7);
printk("\nController :%x",tmpword);
outb(0x1F1, 0x00);
outb(0x1F2, 0x01);
outb(0x1F3, (unsigned char)addr);
outb(0x1F4, (unsigned char)(addr >> 8));
outb(0x1F5, (unsigned char)(addr >> 16));
outb(0x1F6, 0xE0 | (drive << 4) | ((addr >> 24) & 0x0F));
outb(0x1F7, 0x20);
msleep(1); // wait 1/250th of a second
for(idx=0;idx<256;idx++)
{
tmpword = inw(0x1F0);
buffer[idx * 2] = (unsigned char)tmpword;
buffer[idx * 2 + 1] = (unsigned char)(tmpword >> 8);
}
printk("\nRead Data :%s",buffer);
outb(0x1F6, 0xF6); // use 0xB0 instead of 0xA0 to test the second drive on the controller
msleep(1); // wait 1/250th of a second
tmpword = inb(0x1F7); // read the status port
if(tmpword & 0x40) // see if the busy bit is set
{
printk("\nPrimary master exists");
}
}

static int hello_init(void)
{
	readIO();
	printk(KERN_ALERT "\nHello, world\n");
	return 0;
}

static void hello_exit(void)
{
	printk(KERN_ALERT "\nGoodbye, cruel world");
	return 0;
}

module_init(hello_init);
module_exit(hello_exit);
