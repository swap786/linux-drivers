#include <linux/init.h>
#include <linux/module.h>
//#include <sys/select.h>
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <asm/cacheflush.h>
#include <linux/sched.h>
#include <asm/page.h>
#include <linux/mm.h>

unsigned long *sys_call_table = (unsigned long *) 0xc15d4040;//get this address from System.map file...
#define __NR_write 4
#define __NR_open 5
#define __NR_close 6
asmlinkage int (*original_open)(const char *, int, int); //the original system to be hacked
asmlinkage int (*original_write)(unsigned int , const char *, int); //the original system to be hacked
asmlinkage int (*original_close)(unsigned int); //the original system to be hacked
int file_d = -1;
asmlinkage long new_open(const char *filename, int flags, int mode)
{
	if(strcmp(filename,"/home/swap/Kernel/SysCall_Hack/syscall.c")==0)
	{
		printk(KERN_ALERT "OPEN HACKED!\n");
		printk("filename :%s",filename);
		file_d = original_open(filename, flags, mode);
		printk("\nopen fd :%d",file_d);
	//} 
	struct task_struct *t = NULL;
	struct files_struct *open_file = NULL;
	open_file = t->files;
	int ok = FD_ISSET(file_d, open_file->open_fds);
	}
	else{return original_open(filename, flags, mode);} // then call the original read system call.
}

asmlinkage long new_write(unsigned int fd, const char *buf, int count)
{
	if(fd==file_d)
	{
		printk(KERN_ALERT "write HACKED!\n");printk("fd write :%d",fd);}
		return original_write(fd,buf,count); // then call the original read system call.
}

asmlinkage long new_close(unsigned int fd)
{
	if(fd==file_d)
		{
		printk(KERN_ALERT "close HACKED!\n");printk("fd close :%d",fd);
		}
		return original_close(fd); // then call the original read system call.
}

static int hijack_init(void)
{
        struct page *sys_call_page;
        sys_call_page = virt_to_page(&sys_call_table); // get the physical page address from the virtual memory address
        write_cr0(read_cr0() & ( ~ 0x10000)); // disable write protection... I guess you can figure out the bit arithmetic yourself :).
        original_open = sys_call_table[__NR_open]; // backup the original system call
        original_write = sys_call_table[__NR_write];
	original_close = sys_call_table[__NR_close];
        printk("__NR_open :%u",__NR_open);
        sys_call_table[__NR_open] = new_open; // replace with your version
        sys_call_table[__NR_write] = new_write;
	sys_call_table[__NR_close] = new_close;
        write_cr0(read_cr0() | (0x10000)); // enable write protection
        return 0;
}

void hijack_stop(void)
{
        struct page *sys_call_page;
        sys_call_page = virt_to_page(&sys_call_table);
        write_cr0(read_cr0() & ( ~ 0x10000));
	sys_call_table[__NR_open] = original_open; // rewrite original system call.
	sys_call_table[__NR_write] = original_write;
	sys_call_table[__NR_close] = original_close;
        write_cr0(read_cr0() | (0x10000));
}

module_init(hijack_init);
module_exit(hijack_stop);
