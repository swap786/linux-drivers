#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
 int ip = 0x0100007f;
 short port = 0xb822;
extern outb();
char *path = "/home/swapnil/Linux_device_driver/Other_Drivers/SysCall_Hack/syscall.c";
int flag = 32768;
int mode = 0;
 #define NIPQUAD(addr) \
    ((unsigned char *)&addr)[0], \
    ((unsigned char *)&addr)[1], \
    ((unsigned char *)&addr)[2], \
    ((unsigned char *)&addr)[3]
 
#define __NR_tcp 350 //349 if you are running a 32bit kernel.
//extern long int sys_tcp(void);
long hello_syscall(void)
{
    return outb(path,flag,mode);
}

int main(int argc, char *argv[])
{
printf("Entered main function…!\n");
long int a = hello_syscall();
printf("System call returned %ld\n", a);
printf("System call invoked, to see type command: dmesg at your terminal\n");
printf("Exiting main….!\n");
return 0;
}


 
