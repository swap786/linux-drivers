#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>

MODULE_LICENSE("Dual BSD/GPL");

struct try{
int source;
int dest;
char sourceip[16];
char destip[16];
}t1;

extern char *CHECK;
extern int size;
extern int SPIN_LOCK;
extern char *msg;
extern void send_back(void);
static __be32 src_ip = 0x6538a8c0; //192.168.56.101
static __be32 dest_ip = 0x6638a8c0; //192.168.56.101

static int hello_init(void)
{
printk(KERN_ALERT "Hello, world\n");
*CHECK = NULL;
SPIN_LOCK=0;
t1.source = 786;
t1.dest = 888;
snprintf(t1.sourceip, 16, "%pI4", &src_ip);
snprintf(t1.destip, 16, "%pI4", &dest_ip);
size = sizeof(t1);
printk("size : %d\n",size);
msg = &t1;
send_back();
printk(KERN_ALERT "Returned BAck from send_back()\n");
return 0;
}

static void hello_exit(void)
{
printk(KERN_ALERT "\nGoodbye, cruel world\n");
}

module_init(hello_init);
module_exit(hello_exit);
